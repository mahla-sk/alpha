<?php /* Template Name: Landing Page */ ?>
<?php get_header(); ?>

<!--top_container-->

<div class="bg">
<div style="background-color: rgba(0,0,0,0.5);width:100%;height:799;overflow:hidden;">
	 <div class="top_container">
        <div class="c1">
		<h1>گروه برنامه نویسی آلفا</h1>
        <div class="c2">
		   <div class="c3">
		     <p style="letter-spacing:1px;"> توسعه دهنده نرم افزارهای ویندوزی اپلیکشن و طراحی سایت</p>
			 </div>
		   </div>
		   </div>
		  </div>
	<div style="height: 3rem;clear: both;overflow: hidden;line-height: 0;"></div>
		 <!--buttons-->
		 <div class="buttons">
		  <div class="btn_r">
		  <a href="#start" style="color:black;">
		  <span>بزن بریم!</span>
		  <i class="fas fa-walking"></i>
		  </a>
		  <div class="overlay">
		  <a href="#start" style="color:white;">
		  <span>بزن بریم!</span>
		  <i class="fas fa-walking"></i>
		  </a>
		   </div>
		  </div>
		<div class="btn_l">
		  <a href="#samples" style="color:black;">
		  <span>نمونه کارها</span>
		  </a>
		
		</div>
	</div>
		</div>		
	 </div>
	 
	 <div class="clr"></div>
	 
<!--section1-->
  <div class="section1" id="start">
    <div class="sec1">
	  <div class="sec2">
	   <div class="top_sec">
	     <h2 style="text-align:center;color:black;margin-top:114px;">خدمات آلفا</h2>
	   </div>
	  </div>
	</div>
<div class="main_icons" style="margin: 62px -1.5rem;">
  <div class="main_icons1" style="margin: 0 -1.5rem;">
  <div class="icon_container" id="icon_container1" style="margin-right: 66px;">
    <a href="#">
	<div id="icon_container"  style="margin-bottom: -18px;margin-right: 165px;box-shadow:0 0 0 2px #fef557 inset;background-color:#fef557;color:#050000;line-height: 2.5em;height: 90;width: 95;overflow: hidden;border-radius: 50%;transition: box-shadow 0.3s;">
	 <i class="material-icons" style="font-size:45px;margin:23px;">laptop_windows</i>
	 </div>
	</a>
	 <span style="height:20px;"></span>
	 <a class="subject" href="#">
  <h4 style="font-size: 1.5rem;line-height: 1.4;font-weight: 500;letter-spacing: 0em;">توسعه نرم افزار ویندوزی</h4>
     </a>
  <p style="padding-right: 43px;margin: 0 0 1.5rem;color: #525252;margin-top: -29px;font-size: 17px;font-weight: 500;">آلفا در بالاترین سطح کیفی و امنیتی ، نرم افزار های ویندوزی و تحت وب را توسعه میدهد</p>
  </div>
 <div class="icon_container" id="icon_container2">
    <a href="#">
	<div id="icon_container"  style="margin-bottom: -18px;margin-right: 165px;box-shadow:0 0 0 2px #fef557 inset;background-color:#fef557;color:#050000;line-height: 2.5em;height: 90;width: 95;overflow: hidden;border-radius: 50%;transition: box-shadow 0.3s;">
	 <i class="material-icons" style="font-size:45px;margin:23px;">phone_iphone</i>
	 </div>
	</a>
	 <span style="height:20px;"></span>
	  <a class="subject" href="#">
  <h4 style="font-size: 1.5rem;line-height: 1.4;font-weight: 500;letter-spacing: 0em;">توسعه اپلیکیشن</h4>
      </a>
  <p style="padding-right: 43px;margin: 0 0 1.5rem;color: #525252;margin-top: -29px;font-size: 17px;font-weight: 500;">زندگی ما تحت تاثیر گوشی های هوشمند قرار گرفته. داشتن نرم افزار های اندرویدی و IOS برای یک کسب و کار بشدت توصیه میشود.</p>
  </div>
   <div class="icon_container" id="icon_container3">
    <a href="#">
	<div id="icon_container"  style="margin-bottom: -18px;margin-right: 165px;box-shadow:0 0 0 2px #fef557 inset;background-color:#fef557;color:#050000;line-height: 2.5em;height: 90;width: 95;overflow: hidden;border-radius: 50%;transition: box-shadow 0.3s;">
	 <i class="material-icons" style="font-size:45px;margin:23px;">web</i>
	 </div>
	</a>
	 <span style="height:20px;"></span>
	  <a class="subject" href="#">
  <h4 style="font-size: 1.5rem;line-height: 1.4;font-weight: 500;letter-spacing: 0em;">طراحی سایت</h4>
      </a>
  <p style="padding-right: 43px;margin: 0 0 1.5rem;color: #525252;margin-top: -29px;font-size: 17px;font-weight: 500;">طراحی سایت بصورت واکنشگرا و قابل استفاده روی همه ی گجت ها ، طراحی مدرن ، بهینه سازی و تولید محتوا ، بخشی از خدمات آلفا</p>
  </div>
</div>
</div>
  
  </div>
<div class="clr"></div>
   <!--section2-->
   <section class="section2" style="padding-left: 1.5rem;padding-right: 1.5rem;">
   <div style="padding-top:77px;">
    <div class="top_sec2">
	 <div class="top_title">
	    <h2 style="text-align:center;font-size: 40px;font-weight: 400;">در مورد ما بیشتر بدانید</h2>
		<p style="text-align:center;color:#555555;">ما همیشه سعی کردیم تا اعتمادکاربران خود را با ارائه خدمات با کیفیت جلب کنیم.</p>
	 </div>
	</div>
<div style="height: 4rem;"></div>	
     <div class="content_info">
	    <div class="pic_area">
		 <div class="pic">
		  <img width="515" height="280" src="https://alphacorp.ir/wp-content/uploads/2019/08/next4-1024x557.jpg"> 
		 </div>
		 </div>
	 <div class="sec2_info" style="color:#525252;">
	    <div class="sec2_container" style="text-align: right;margin-right: 714px;width: 37%;margin-top: -372px;">
		 <div class="sec2_">
		    <div class="mi">
			 <i class="material-icons" style="color:black;font-size: 38px;float: right;margin-right: -23px;">contact_support</i>
			</div>
			<div class="sec_2" style="margin-top: 54px;margin-right: 17px;">
			 <h4 style="font-size: 20px;margin-right: 21px;">پشتیبانی سریع از محصولات</h4>
		    <p style="margin-right: 18px;">ما از شما ، بصورت شبانه روزی و سریع ، آنلاین یا حضوری در هر جای ایران پشتیبانی میکنیم.</p>
		 </div>
		 </div>
		</div>
		<div style="height: 3rem;"></div>
	 </div>
	<div class="sec2_info" style="color:#525252;">
	    <div class="sec2_container" style="text-align: right;margin-right: 714px;width: 37%;margin-top: -89px;">
		 <div class="sec2_">
		    <div class="mi">
			 <i class="far fa-smile-beam" style="color:black;font-size: 38px;float: right;margin-right: -23px;"></i>
			</div>
			<div class="sec_2" style="margin-top: 54px;margin-right: 17px;">
			 <h4 style="font-size: 20px;margin-right: 21px;">رضایت مشتریان</h4>
		    <p style="margin-right: 18px;">آلفا بدنبال کسب رضایت مشتریان خود است. اعتماد متقابل یکی از نقاط قوت تیم آلفاست.</p>
		 </div>
		 </div>
		</div>
		<div style="height: 3rem;"></div>
	 </div>
	<div class="sec2_info" style="color:#525252;">
	    <div class="sec2_container" style="text-align: right;margin-right: 714px;width: 37%;margin-top: -85px;">
		 <div class="sec2_">
		    <div class="mi">
			 <i class="material-icons" style="color:black;font-size: 38px;float: right;margin-right: -23px;">security</i>
			</div>
			<div class="sec_2" style="margin-top: 54px;margin-right: 17px;">
			 <h4 style="font-size: 20px;margin-right: 21px;">امنیت</h4>
		    <p style="margin-right: 18px;">تیم امنیت شبکه آلفا ، حفظ حریم خصوصی و امنیت شما را تضمین میکند.</p>
		 </div>
		 </div>
		</div>
		<div style="height: 3rem;"></div>
	 </div>
	 </div>
	 </div>
   </section>
  <!--section3-->
  <section class="section3">
  <div style="background-color: rgba(0,0,0,0.5);width:100%;height:880;overflow:hidden;background-attachment:fixed;">
    <div class="sec3_">
	 <div style="margin-top: 0px !important;margin-bottom: 5px !important;">
	    <h2 style="text-align:center;color:white;font-size: 38px;margin-top: 125px;font-weight: 400;">نمونه کارها</h2>
	 </div>
	 <div style="height:3 rem"></div>
	<div style="margin-top:70px;margin-right: 101px;">
	  <div class="sec3-contenttop">
	    <div class="pictures">
		 <a href="#">
		    <img id="img2" src="https://alphacorp.ir/wp-content/uploads/2020/10/Untitled-2.jpg" >
		 </a>
		</div>
		<div class="pictures">
		 <a href="#">
		    <img id="img2" src="https://alphacorp.ir/wp-content/uploads/2019/08/goldoon.jpg" >
		 </a>
		</div>
		<div class="pictures">
		 <a href="#">
		    <img id="img3" src="https://alphacorp.ir/wp-content/uploads/2019/08/dooocjpg.jpg">
		 </a>
		</div>
	  </div>
	<div class="sec3-contentbottom">
	    <div class="pictures">
		 <a href="#">
		    <img id="img4" src="https://alphacorp.ir/wp-content/uploads/2019/08/gazar1.jpg" >
		 </a>
		</div>
		<div class="pictures">
		 <a href="#">
		    <img id="img5" src="https://alphacorp.ir/wp-content/uploads/2015/07/mohsen-2-1024x614.jpg" >
		 </a>
		</div>
		<div class="pictures">
		 <a href="#">
		    <img id="img6" src="https://alphacorp.ir/wp-content/uploads/2015/07/bazoo1-1.jpg" >
		 </a>
		</div>
	</div>
	</div>
</div>	
	<div class="clr"></div>
  </div>
  </section>
 <!--section4-->
 <section class="section4">
    <div style="top: 2870;width: 100%;">
	 <div style="top: 2870;width: 100%;">
	   <div class="text_part">
	     <h2>سخنی با شما</h2>
		 <p style="color:#575757;font-size:18;">افتخار ما انتخاب شماست<br>
بسیار خرسندیم از این که همراه ما هستید.<br>
گروه نرم افزاری آلفا متشکل از تیم جوان و متعهد، با علم به وجود شرکت های متعدد فعال در زمینه نرم افزاری,وارد این عرصه شده و قصد دارد با خدمات ویژه خود تفاوت را برای شما بوجود آورده و رضایت شما را جلب نماید و همواره رضایت مشتری را در صدر اصول خود قرار داده است.<br>
تیم آلفا در زمینه های زیر آماده خدمت رسانی ب شما همراهان عزیز می باشد :<br>
– طراحی و توسعه نرم افزار های اندرویدی, ویندوزی و ios<br>
– طراحی و توسعه وبسایت<br>
– طراحی و توسعه بازی های اندرویدی و ویندوزی در سبک های دوبعدی و سه بعدی</p>
	   
	   </div>
	   <div class="pic_part">
	    <img src="https://alphacorp.ir/wp-content/uploads/2019/02/alphacorp.jpg" width="570" height="327">
	   </div>
	 </div>
	</div>
 </section>
 <!--section5-->
 <div class="section5"><div style="background-color: rgba(0,0,0,0.5);width:100%;height:335;overflow:hidden;background-attachment:fixed;"></div></div>


 <?php get_footer(); ?>