<!--section6-->
 <section class="section6">
    <div class="sec_6" style="margin-top: 99px;margin-right: 30px;">
	 <div class="sec6_container">
	     <div class="sec6__" id="sec6__1">
		    <div class="i_container">
		    <i class="material-icons">thumb_up</i>
			</div>
			<h4 class="sec6_h4">رضایتمندی بالای مشتریان</h4>
		 </div>
		 <div class="clr"></div>
		  <div class="sec6__" id="sec6__2">
		    <div class="i_container">
		    <i class="material-icons">star</i>
			</div>
			<h4 class="sec6_h4">بالاترین کیفیت</h4>
		 </div>
		 <div class="clr"></div>
	 </div>
	 <div class="sec6_container" style="margin-top: -256px;margin-right: 494px;">
	     <div class="sec6__" id="sec6__3">
		    <div class="i_container">
		    <i class="material-icons">color_lens</i>
			</div>
			<h4 class="sec6_h4">رابط کاربری زیبا</h4>
		 </div>
		 <div class="clr"></div>
		  <div class="sec6__" id="sec6__4">
		    <div class="i_container">
		    <i class="material-icons">settings</i>
			</div>
			<h4 class="sec6_h4">پشتیبانی فنی سریع</h4>
		 </div>
		 <div class="clr"></div>
	 </div>
	 <div class="sec6_container" style="margin-top: -256px;margin-right: 915px;">
	     <div class="sec6__" id="sec6__5">
		    <div class="i_container">
		    <i class="material-icons">spellcheck</i>
			</div>
			<h4 class="sec6_h4">بهترین سئو و بهینه سازی</h4>
		 </div>
		 <div class="clr"></div>
		  <div class="sec6__">
		    <div class="i_container" id="isec6">
		    <i class="material-icons" >tablet_android</i>
			</div>
			<h4 class="sec6_h4" id="sec6_h4">تمام رسپانسیو</h4>
		 </div>
		 <div class="clr"></div>
	 </div>
	<div class="clr"></div>
	</div>
	<section class="footer" style="margin-top:-20px;">
    <div>
	 <p style="color:#424242;margin:7px;padding: 36px;">تمامی حقوق برای مجموعه آلفا محفوظ است.</p>
	</div>
	<div class="icon_footer">
	     <i class="fab fa-instagram" style="color:#393939;margin-left: 32px;"></i>
		 <i class="fab fa-linkedin" style="margin-left: 24px;"></i>
		 <i class="fab fa-telegram" style="margin-left: 26px;"></i>
	
	</div>
 
 </section>
	</section>