<DOCTYPE! html>
<html class="no-touch" dir="rtl" lang="fa-IR">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> گروه</title>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/new 1.css">
</head>

<body>
<!--topdiv-->
     <div class="top_div">
	    <div class="info_sec">
		 <span><i class="material-icons" style="color:white;font-size:18px;">local_phone</i></span>&nbsp
		 <span style="color:white;font-size:19px;">989390299876+</span>&nbsp&nbsp&nbsp&nbsp&nbsp
		 <span><i class="material-icons" style="color:white;font-size:18px;">mail</i></span>&nbsp
		 <a href="mailto:info@test.com" style="color:white;font-size:19px;"><span>contact@antus.ir</span></a>
		 <i class="fab fa-telegram" style="margin-left: 80px;"></i>
		 <i class="fab fa-instagram" style="margin-left: 24px;"></i>
		 <i class="fab fa-linkedin" style="margin-left: 26px;"></i>
		
		</div>
		<div class="clr"></div>
     </div>
	 <div class="clr"></div>
<!--menu-->	 
	 <nav>
	     <div class="rsides">
		 <span class="nav_pple"><i class="material-icons" style="color:white;font-size: 29px;margin-right: 77px;margin-top: 19px;">people</i></span>&nbsp
		 <a href="https://alphacorp.ir/"><span style="color:white;font-size: 19px;">گروه برنامه نویسی آلفا</span></a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		 <a href="https://alphacorp.ir/"><span id="home_btn">خانه</span></a>
		 <span class="nav_srch"><i class="material-icons" id="srch">search</i></span>
		 
		 </div>
	 <div class="clr"></div>
	 </nav>
	 <div class="clr"></div>